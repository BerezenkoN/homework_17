package Bank;


import java.sql.*;
import java.util.List;
import java.util.ArrayList;
import java.sql.Date;


/**
 * Created by user on 24.11.2016.
 */
public class CustomersOperations {
    static Connection connection  = null;

    public static void selectAllCustomers()throws SQLException {
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT * FROM customers");
        while(! rs.isLast()) {
            rs.next();
            printCustomer(rs);
        }
        stmt.close();
        }
        public static List<Long> selectCustomersId(Connection connection) throws SQLException{
            List<Long> idCustomers = new ArrayList<Long>();
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT id FROM customers");
//            SELECT * FROM customers
            while(! rs.isLast()) {
                rs.next();
//                idCustomers.add(rs);
                idCustomers.add(rs.getLong("id"));
            }
            stmt.close();
            return idCustomers;
        }
    public static void selectCustomersByPassport(String passport) throws SQLException {
        PreparedStatement ps = connection.prepareStatement("SELECT * FROM customers WHERE passport = ?");
        ps.setString(1, passport);
        ResultSet rs = ps.executeQuery();
        while (!rs.isLast()) {
            rs.next();
            printCustomer(rs);
        }
        ps.close();
    }
    public static void constantInsert() throws SQLException {
        Statement stmt = connection.createStatement();
        stmt.executeUpdate("INSERT INTO customers (firsName, lastname, birthdate, addresses, city, passport, phone) VALUES ('Mikhail', 'Kazancev', '1988-12-18', 'Shevchenko,127', 'Dnipro', 'AH217149', '0987891011')");
        stmt.close();
    }

    public static void smartInsert(String firstName,String lastName,String birthdate,String addresses,
                                   String city,String passport,String phone) throws SQLException {
        PreparedStatement ps = connection.prepareStatement("INSERT INTO customers (firsName, lastname, birthdate, addresses, city, passport, phone) VALUES(?, ?, ?, ?, ?, ?, ?)");
        ps.setString(1, firstName);
        ps.setString(2, lastName);
        ps.setDate(3, Date.valueOf(birthdate));
        ps.setString(4, addresses);
        ps.setString(5, city);
        ps.setString(6, passport);
        ps.setString(7, phone);
        ps.executeUpdate();
        ps.close();
    }
    public static void updateCustomerFirstAndLastName(Long id, String firstName, String lastName) throws SQLException {
        PreparedStatement ps = connection.prepareStatement("UPDATE customers SET firstName = ?, lastName = ?, WHERE id = ? ");
        ps.setString(1, firstName);
        ps.setString(2, lastName);
        ps.setLong(3, id);
        ps.executeUpdate();
        ps.close();
    }
    public static void deleteCustomer(Long id)throws SQLException {
        PreparedStatement ps = connection.prepareStatement("DELETE FROM customers WHERE id = ?");
        ps.setLong(1, id);
        ps.executeUpdate();
        ps.close();
    }
    private static void printCustomer(ResultSet result)throws SQLException{
        Long id = result.getLong("id");
        String firstName = result.getString("firstName");
        String lastName = result.getString("lastName");
        Date birthdate = result.getDate("birthdate");
        String addresses = result.getString(("addresses"));
        String city = result.getString("city");
        String passport = result.getString("passport");
        String phone =result.getString("phone");

        System.out.println("id" + id + "firstName" + firstName + "lastName" + lastName + "birthdate" + birthdate + "addresses" + addresses + "city" + city + "passport" + passport + "phone" + phone);
    }
    public static void main(String[] args) throws SQLException {
        try {
            connection = ConnectionUtil.createConnection();
            constantInsert();
            smartInsert("Mikhail", "Kazancev", "1988-12-18", "Shevchenko,127", "Dnipro", "AH217149", "0987891011");
            selectAllCustomers();
            selectCustomersByPassport("passport");
            updateCustomerFirstAndLastName(1L, "Emery", "Unay");
            selectAllCustomers();
            deleteCustomer(1L);
        } catch (Exception e){
            e.printStackTrace();

        }finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            }catch(SQLException e){
                e.printStackTrace();

                }
            }

        }






    }










