package Bank;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 27.11.2016.
 */
public class TransactionsOperations {
    static Connection connection  = null;
    public static void main(String[] args) {
        connection = ConnectionUtil.createConnection();
    }
    public static void batchTransactionsInsert() throws SQLException{
        PreparedStatement ps = connection.prepareStatement("INSERT INTO transactions (amount, date," +
                " operationtype, accountnumber) VALUES (?, ?, ?, ?)");
        for(Long accountnumber:AccountsOperations.selectAccountsId(connection)) {
            ps.setBigDecimal(1, new BigDecimal(100));
            ps.setTimestamp(2, java.sql.Timestamp.valueOf(java.time.LocalDateTime.now()));
            ps.setString(3, "PUT");
            ps.setLong(4, accountnumber);
            ps.addBatch();
        }
        ps.executeBatch();
        ps.close();
    }
    public static void updateAllTransactions()throws SQLException {
        Statement st = connection.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE);
        ResultSet rs = st.executeQuery("SELECT * FROM transactions");
        while (!rs.isLast()) {
            rs.next();
            String operationtype = rs.getString("operationtype");
            if ("PUT".equals(operationtype)) {
                rs.updateBigDecimal("amount", new BigDecimal(150));
                rs.updateRow();
            }
        }
        rs.close();
        st.close();
    }
    public static void deleteAllWithdraw() throws SQLException{
        List<Long> accountsNumder = new ArrayList<Long>();
        Statement st = connection.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE);
        ResultSet rs = st.executeQuery("SELECT * FROM transactions");
        while(! rs.isLast()) {
            rs.next();
            String operationtype = rs.getString("operationtype");
            if ("WSD".equals(operationtype)) {
                accountsNumder.add(rs.getLong("accountnumber"));
                rs.deleteRow();
            }
        }
        for(Long accountnumber : accountsNumder) {
            rs.moveToInsertRow();
            rs.updateBigDecimal("amount", new BigDecimal(100));
            rs.updateTimestamp("date", java.sql.Timestamp.valueOf(java.time.LocalDateTime.now()));
            rs.updateString("operationtype", "PUT");
            rs.updateLong("accountnumber", accountnumber);
            rs.insertRow();
        }
        rs.moveToCurrentRow();
        rs.close();
        st.close();




        }


}

