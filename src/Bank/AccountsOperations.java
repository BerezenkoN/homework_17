package Bank;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 27.11.2016.
 */
public class AccountsOperations {
   static Connection connection = null;
    public static void main(String[] args) {
        connection = ConnectionUtil.createConnection();
            try {
                smartInsert(new BigDecimal(17686), "1997-12-31 23:59:59", "UAH", false, 1L);
                selectAccounts();
            } catch (SQLException e) {
                e.printStackTrace();
            }finally {

                try {
                    if (connection != null) {
                        connection.close();
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }

            }
        }
    private static void printAccount(ResultSet result) throws SQLException{
        Long accountnumber = result.getLong("accountnumber");
        BigDecimal balance = result.getBigDecimal ("balance");
        String creationdate = result.getString ("creationdate");
        String currency = result.getString ("currency");
        boolean blocked = result.getBoolean("blocked");
        Long customerid = result.getLong ("customerid");


        System.out.println("accountnumber" + accountnumber + "balance" + balance + "creationdate" + creationdate + "currency" + currency + "blocked" + blocked + "customerid" + customerid);
    }
    public static void selectAccounts() throws SQLException{
        Statement st = connection.createStatement();
                ResultSet rs = st.executeQuery("SELECT * FROM accounts");
        while(! rs.isLast()) {
            rs.next();
            printAccount(rs);
        }
        st.close();
    }
    public static List<Long> selectAccountsId(Connection connection) throws SQLException{
        List<Long> accountsNumder = new ArrayList<Long>();
        Statement st = connection.createStatement();
        ResultSet rs = st.executeQuery("SELECT * FROM accounts");
        while(! rs.isLast()) {
            rs.next();
            accountsNumder.add(rs.getLong("accountnumber"));
        }
        st.close();
        return accountsNumder;
    }

    public static void smartInsert(BigDecimal balance,String creationdate,String currency,boolean blocked,Long customerid) throws SQLException{
        PreparedStatement ps = connection.prepareStatement("INSERT INTO accounts (balance, creationdate, currency, blocked, customerid) VALUES(?, ?, ?, ?, ?)");
        ps.setBigDecimal(1, balance);
        ps.setTimestamp(2, java.sql.Timestamp.valueOf(creationdate));
        ps.setString(3, currency );
        ps.setBoolean(4, blocked);
        ps.setLong(5, customerid);
        ps.executeUpdate();
        ps.close();
    }
    public static void batchAccountsInsert()throws SQLException{
        PreparedStatement ps = connection.prepareStatement("INSERT INTO accounts (creationdate, customerid) VALUES (?, ?)");
        for(Long customerid: CustomersOperations.selectCustomersId(connection)) {
            ps.setTimestamp(1, java.sql.Timestamp.valueOf(java.time.LocalDateTime.now()));
            ps.setLong(2,customerid);
            ps.addBatch();
            ps.close();
        }

    }
    }


